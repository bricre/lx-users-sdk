# Official Logistics-X.com API SDK (PHP) - users module

Code generated from Logistics-X.com's OpenAPI file
using [allansun/openapi-code-generator](https://github.com/allansun/openapi-code-generator).

Generated code is well self-documented with proper PHPDoc annotations.

Please refer to [Logistics-X.com's documentation](http://www.logistics-x.com/api/users/docs) for detailed API 
behaviour explanation.

## Installation

```shell
composer require logistics-x/users-sdk
```

You will also need a [PSR-18 compatible client](https://www.php-fig.org/psr/psr-18/) see
[https://docs.php-http.org/en/latest/clients.html](https://docs.php-http.org/en/latest/clients.html)

So either use Guzzle (or any other PSR-18 compatible clients)

```shell
composer require php-http/guzzle7-adapter
```

## Versioning

This project matches Logistics-X's API versioning.

Should you found a matching version not being available. Please contact the author to generate against correct version.

## Usage

First you need to create a PSR-18 Then in your business logic you can call API operations directly

```php
<?php
use LogisticsX\Users\Api\User;
$httpClient = new \GuzzleHttp\Client([
    'base_uri' => 'https://www.logistics-x.com/',
    'headers'=>[
        'Authorization'=> 'Bearer <accessToken>'
    ]
]);
$api = new User($httpClient);

$users = $api->getUserCollection();

```

## Author

* [Allan Sun](https://github.com/allansun) - *Initial work*
