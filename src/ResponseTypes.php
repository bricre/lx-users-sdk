<?php

namespace LogisticsX\Users;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'getJwtToken' => [
            '200.' => 'LogisticsX\\Users\\Model\\Token',
        ],
        'refreshToken' => [
            '200.' => 'LogisticsX\\Users\\Model\\Token',
        ],
        'getUserCollection' => [
            '200.' => 'LogisticsX\\Users\\Model\\User\\Read[]',
        ],
        'postUserCollection' => [
            '201.' => 'LogisticsX\\Users\\Model\\User\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'statusStatisticsUserCollection' => [
            '200.' => 'LogisticsX\\Users\\Model\\User\\Statistics[]',
        ],
        'getUserItem' => [
            '200.' => 'LogisticsX\\Users\\Model\\User\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putUserItem' => [
            '200.' => 'LogisticsX\\Users\\Model\\User\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteUserItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
    ];
}
