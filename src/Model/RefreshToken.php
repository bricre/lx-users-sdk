<?php

namespace LogisticsX\Users\Model;

use OpenAPI\Runtime\AbstractModel;

class RefreshToken extends AbstractModel
{
    /**
     * @var string
     */
    public $token = null;

    /**
     * @var string[]
     */
    public $user = null;

    /**
     * @var string
     */
    public $refresh_token = null;
}
