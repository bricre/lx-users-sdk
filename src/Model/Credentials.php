<?php

namespace LogisticsX\Users\Model;

use OpenAPI\Runtime\AbstractModel;

class Credentials extends AbstractModel
{
    /**
     * @var string
     */
    public $username = null;

    /**
     * @var string
     */
    public $password = null;
}
