<?php

namespace LogisticsX\Users\Model\User;

use OpenAPI\Runtime\AbstractModel;

/**
 * User.
 */
class Write extends AbstractModel
{
    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var string|null
     */
    public $nickname = null;

    /**
     * @var string
     */
    public $username = null;

    /**
     * @var string
     */
    public $password = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';
}
