<?php

namespace LogisticsX\Users\Model\User;

use OpenAPI\Runtime\AbstractModel;

/**
 * User.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var string|null
     */
    public $nickname = null;

    /**
     * @var string
     */
    public $username = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     */
    public $updateTime = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    /**
     * @var string[]
     */
    public $roles = null;
}
