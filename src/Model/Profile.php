<?php

namespace LogisticsX\Users\Model;

use OpenAPI\Runtime\AbstractModel;

class Profile extends AbstractModel
{
    /**
     * @var string
     */
    public $username = null;

    /**
     * @var int
     */
    public $clientId = null;
}
