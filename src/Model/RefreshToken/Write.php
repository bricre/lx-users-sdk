<?php

namespace LogisticsX\Users\Model\RefreshToken;

use OpenAPI\Runtime\AbstractModel;

class Write extends AbstractModel
{
    /**
     * @var string
     */
    public $refresh_token = null;
}
