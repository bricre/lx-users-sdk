<?php

namespace LogisticsX\Users\Api;

use LogisticsX\Users\Model\Credentials;
use LogisticsX\Users\Model\RefreshToken\Write;
use LogisticsX\Users\Model\Token as TokenModel;

class Token extends AbstractAPI
{
    /**
     * Get JWT Token with given credential.
     *
     * @param Credentials $Model login info
     *
     * @return TokenModel
     */
    public function getJwt(Credentials $Model): TokenModel
    {
        return $this->request(
        'getJwtToken',
        'POST',
        'api/users/token',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * After requesting /api/users/token, a `refresh_token` field will be returned, you
     * can use this token to get a new token with longger expiry time.
     *
     * @param Write $Model Refresh token
     *
     * @return TokenModel
     */
    public function refresh(Write $Model): TokenModel
    {
        return $this->request(
        'refreshToken',
        'POST',
        'api/users/token/refresh',
        $Model->getArrayCopy(),
        [],
        []
        );
    }
}
