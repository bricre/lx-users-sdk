<?php

namespace LogisticsX\Users\Api;

use LogisticsX\Users\Model\User\Read;
use LogisticsX\Users\Model\User\Statistics;
use LogisticsX\Users\Model\User\Write;

class User extends AbstractAPI
{
    /**
     * Get User Profile.
     */
    public function getProfile()
    {
        return $this->request(
        'getUserProfile',
        'GET',
        'api/users/profile',
        null,
        [],
        []
        );
    }

    /**
     * Retrieves the collection of User resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'username'	string
     *                       'nickname'	string
     *                       'clientCode'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'id[between]'	string
     *                       'id[gt]'	string
     *                       'id[gte]'	string
     *                       'id[lt]'	string
     *                       'id[lte]'	string
     *                       'clientId[between]'	string
     *                       'clientId[gt]'	string
     *                       'clientId[gte]'	string
     *                       'clientId[lt]'	string
     *                       'clientId[lte]'	string
     *                       'clientCode[between]'	string
     *                       'clientCode[gt]'	string
     *                       'clientCode[gte]'	string
     *                       'clientCode[lt]'	string
     *                       'clientCode[lte]'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'exists[clientId]'	boolean
     *                       'exists[clientCode]'	boolean
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[clientId]'	string
     *                       'order[clientCode]'	string
     *                       'order[username]'	string
     *                       'order[nickname]'	string
     *                       'order[status]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getUserCollection',
        'GET',
        'api/users/users',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a User resource.
     *
     * @param Write $Model The new User resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postUserCollection',
        'POST',
        'api/users/users',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of User resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'username'	string
     *                       'nickname'	string
     *                       'clientCode'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'id[between]'	string
     *                       'id[gt]'	string
     *                       'id[gte]'	string
     *                       'id[lt]'	string
     *                       'id[lte]'	string
     *                       'clientId[between]'	string
     *                       'clientId[gt]'	string
     *                       'clientId[gte]'	string
     *                       'clientId[lt]'	string
     *                       'clientId[lte]'	string
     *                       'clientCode[between]'	string
     *                       'clientCode[gt]'	string
     *                       'clientCode[gte]'	string
     *                       'clientCode[lt]'	string
     *                       'clientCode[lte]'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'exists[clientId]'	boolean
     *                       'exists[clientCode]'	boolean
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[clientId]'	string
     *                       'order[clientCode]'	string
     *                       'order[username]'	string
     *                       'order[nickname]'	string
     *                       'order[status]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return Statistics[]|null
     */
    public function statusStatisticsCollection(array $queries = []): ?array
    {
        return $this->request(
        'statusStatisticsUserCollection',
        'GET',
        'api/users/users/statistics',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a User resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getUserItem',
        'GET',
        "api/users/users/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the User resource.
     *
     * @param string $id    Resource identifier
     * @param Write  $Model The updated User resource
     *
     * @return Read
     */
    public function putItem(string $id, Write $Model): Read
    {
        return $this->request(
        'putUserItem',
        'PUT',
        "api/users/users/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the User resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteUserItem',
        'DELETE',
        "api/users/users/$id",
        null,
        [],
        []
        );
    }
}
