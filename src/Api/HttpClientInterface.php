<?php

namespace LogisticsX\Users\Api;

use Psr\Http\Client\ClientInterface as BaseClass;

interface HttpClientInterface extends BaseClass
{
}
